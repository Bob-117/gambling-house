# Gambling House :


```mermaid
%%{ init : { "theme" : "base", "flowchart" : { "curve" : "basic" }}}%%
graph TB;
    classDef Object fill:#337AFF;
    classDef File fill:#F6FF33;
    classDef User fill:#33FF6E;
    
   main["main.py"]:::File
   lang["assets"]:::File
   casino["CASINO"]:::Object
   dealer["DEALER"]:::Object
   uiin["UI"]:::Object
   uiout["UI"]:::Object
   uitk["UI_TK"]:::Object
   uicli["UI_CLI"]:::Object
   dealer["DEALER"]:::Object
   rules["RULES"]:::Object
   player["PLAYER"]:::Object
   ai["AI"]:::Object
   human["HUMAN"]:::Object
   card["CARD"]:::Object
   room["ROOM"]:::Object
   game["GAME"]:::Object
   round["ROUND"]:::Object
    
    
   subgraph APP
       main -- load --> lang --> main
       main -- open --> casino
       subgraph CASINO
       casino --> uiin --> uicli & uitk --> uiout 
           subgraph UI
               uiout --> dealer
               dealer --> rules --> room
               dealer --> player --> ai & human --> room
               dealer -->  card --> room
               subgraph ROOM
               room --> game --> round -- loop --> round
               round -. replay .->  room
               end
           end
       end
       room -. save .->  dealer
      subgraph DB
       casino --> DATABASE
      end
   end
   
    
```
    main -- "load_language(MAIN_LANGUAGE)" --> main
    main -- "load_display_mode(MAIN_LANGUAGE)" --> main
InitPy["python3 main.py"]
InitDocker["docker run -e MAIN_LANGUAGE='EN' -e DISPLAY_MODE='CLI' gambling_house:1.0.0"]
InitPy & InitDocker --> main
ui["UI"]
uitk["UI_TK"]
uicli["UI_CLI"]
dealer["DEALER"]
room["ROOM"]
rules["RULES"]
card["CARD"]
main --> casino --> ui --> uitk & uicli --> dealer
rules --> dealer
dealer --> generate_card --> dealer
dealer --> room --> game --> round
card --> generate_card
round -- loop --> round
round --> game
game --> dealer
```mermaid
erDiagram

    CASINO ||--o{ DEALER : "user ask"
    DEALER ||--o{ ROOM : "open"
    
    CASINO{
        string name
    }
    DEALER{
        string name
    }
    ROOM{
        string game_name
        object RULES
        
    }
    GAME{
        string name
    }
    CARD{
        string name
    }
    PLAYER{
        string name
        list hand CARD "[Card]"
    }
    
```




flowchart TB
InitPy["python3 main.py"]
InitDocker["docker run -e MAIN_LANGUAGE='EN' -e DISPLAY_MODE='CLI' gambling_house:1.0.0"]
Casino["CASINO"]
open["open()"]
cli["CLI UI"]
tk["TK UI"]
ui["UI"]
Game["GAME"]
Game1["start()"]
Round["Round class (recursive)"]

    Casino-- display_mode & \n language selection -->open
    open-- select a game\n register players --> cli & tk --> ui
    ui -- init game and register players --> Game
    Game --> Game1
    Game1 --> Round -- loop while len winnners != len players -1 --> Round
    Round -- end game --> UI


